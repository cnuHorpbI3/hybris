package by.epam.hybris.daos;

import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

public interface UserDAO {

    List<UserModel> findUsers();

    List<UserModel> findUsersByName(String name);
}

