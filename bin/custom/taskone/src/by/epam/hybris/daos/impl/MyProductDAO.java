package by.epam.hybris.daos.impl;

import by.epam.hybris.daos.ProductDAO;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class MyProductDAO implements ProductDAO {

    @Autowired
    private FlexibleSearchService flexibleSearchService;


    @Override
    public List<ProductModel> getAllProducts() {
        final String queryString = "SELECT {p:" + ProductModel.PK + "}"
                + "FROM {" + ProductModel._TYPECODE + " AS p} ";

        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);

        return flexibleSearchService.<ProductModel>search(query).getResult();
    }
}
