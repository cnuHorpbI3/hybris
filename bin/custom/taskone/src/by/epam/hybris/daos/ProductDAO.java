package by.epam.hybris.daos;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

public interface ProductDAO {

    List<ProductModel> getAllProducts();
}
