package by.epam.hybris.daos.impl;

import by.epam.hybris.daos.UserDAO;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(value = "userDAO")
public class DefaultUserDAO implements UserDAO {

    @Autowired
    private FlexibleSearchService flexibleSearchService;


    @Override
    public List<UserModel> findUsers() {

        final String queryString = "SELECT {p:" + UserModel.PK + "} FROM {" + UserModel._TYPECODE + " AS p} ";

        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);

        return flexibleSearchService.<UserModel>search(query).getResult();
    }

    @Override
    public List<UserModel> findUsersByName(String name) {
        final String queryString = "SELECT {p:" + UserModel.PK + "}"
                + "FROM {" + UserModel._TYPECODE + " AS p} "
                + "WHERE " + "{p:" + UserModel.NAME + "}=?name ";

        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
        query.addQueryParameter("name", name);

        return flexibleSearchService.<UserModel>search(query).getResult();
    }
}
