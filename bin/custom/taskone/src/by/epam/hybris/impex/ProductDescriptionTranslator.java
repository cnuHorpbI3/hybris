package by.epam.hybris.impex;

import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloInvalidParameterException;

import java.util.HashMap;
import java.util.Map;

public class ProductDescriptionTranslator extends AbstractValueTranslator {

    @Override
    public Object importValue(String value, Item item) throws JaloInvalidParameterException {
        try {
            Map<Integer, String> description = (HashMap<Integer, String>) item.getAttribute("description");
            String descriptionAttributeValue = description.entrySet().iterator().next().getValue();

            return descriptionAttributeValue + value;
        } catch (JaloBusinessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String exportValue(Object o) throws JaloInvalidParameterException {
        return (String) o;
    }
}
