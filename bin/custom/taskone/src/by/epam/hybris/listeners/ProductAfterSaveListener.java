package by.epam.hybris.listeners;

import by.epam.hybris.services.MyProductService;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.AfterSaveEvent;
import de.hybris.platform.tx.AfterSaveListener;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;

public class ProductAfterSaveListener implements AfterSaveListener {

    private ModelService modelService;
    private MyProductService productService;

    @Override
    public void afterSave(final Collection<AfterSaveEvent> events) {
        for (final AfterSaveEvent event : events)
        {
            final int type = event.getType();
            if (AfterSaveEvent.UPDATE == type)
            {
                final PK pk = event.getPk();

                if (1 == pk.getTypeCode())
                {
                    final ProductModel product = modelService.get(pk);
                    productService.startChangeWorkflow(product);
                }
            }
        }
    }

    @Required
    public void setModelService(final ModelService modelService)
    {
        this.modelService = modelService;
    }

    @Required
    public void setProductService(final MyProductService productService) {
        this.productService = productService;
    }
}
