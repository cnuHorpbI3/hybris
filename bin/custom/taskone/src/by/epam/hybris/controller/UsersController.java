package by.epam.hybris.controller;

import by.epam.hybris.data.UserData;
import by.epam.hybris.facades.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class UsersController {
    private UserFacade userFacade;

    @Autowired
    public void setUserFacade(final UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @RequestMapping(value = "/users")
    public String showUsers(final Model model) {
        final List<UserData> users = userFacade.getAllUsers();
        model.addAttribute("users", users);

        return "UserListing";
    }
}
