package by.epam.hybris.setup;

import by.epam.hybris.constants.TaskoneConstants;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.impex.jalo.ImpExManager;
import de.hybris.platform.util.CSVConstants;

import java.io.InputStream;

@SystemSetup(extension = TaskoneConstants.EXTENSIONNAME)
public class CoreSystemSetup {

    private static final String CUSTOM_UPDATE_IMPEX_PATH = "impex/custom-update-description.impex";
    private static final String CUSTOM_INIT_IMPEX_PATH = "impex/custom-init-description.impex";

    @SystemSetup(process = SystemSetup.Process.INIT)
    public static void setupInit() {
        loadImpex(CUSTOM_INIT_IMPEX_PATH);
    }

    @SystemSetup(process = SystemSetup.Process.UPDATE)
    public static void setupUpdate() {
        loadImpex(CUSTOM_UPDATE_IMPEX_PATH);
    }

    private static void loadImpex(String impexPath) {
        InputStream is = CoreSystemSetup.class.getClassLoader().getResourceAsStream(impexPath);
        ImpExManager.getInstance().importData(is, "windows-1252",
                CSVConstants.HYBRIS_FIELD_SEPARATOR, CSVConstants.HYBRIS_QUOTE_CHARACTER, true);
    }
}
