package by.epam.hybris.services;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

public interface MyProductService {

    List<ProductModel> getAllProducts();

    void startChangeWorkflow(ProductModel productModel);
}
