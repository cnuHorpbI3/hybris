package by.epam.hybris.services;

import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

public interface UserService {

    List<UserModel> getUsers();

    List<UserModel> getUsersForName(String name);
}
