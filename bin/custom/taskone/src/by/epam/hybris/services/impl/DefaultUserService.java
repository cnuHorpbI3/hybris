package by.epam.hybris.services.impl;

import by.epam.hybris.services.UserService;
import by.epam.hybris.daos.UserDAO;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public class DefaultUserService implements UserService {

    private UserDAO userDAO;

    @Override
    public List<UserModel> getUsers() {
        return userDAO.findUsers();
    }

    @Override
    public List<UserModel> getUsersForName(String name) {
        final List<UserModel> result = userDAO.findUsersByName(name);

        if (result.isEmpty()) {
            throw new UnknownIdentifierException("Users with name '" + name + "' not found!");
        }

        return result;
    }

    @Required
    public void setUserDAO(final UserDAO userDAO) {
        this.userDAO = userDAO;
    }
}
