package by.epam.hybris.services.impl;

import by.epam.hybris.daos.ProductDAO;
import by.epam.hybris.services.MyProductService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.workflow.WorkflowProcessingService;
import de.hybris.platform.workflow.WorkflowService;
import de.hybris.platform.workflow.WorkflowTemplateService;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowModel;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public class MyProductServiceImpl implements MyProductService {

    @Autowired
    private WorkflowService workflowService;
    @Autowired
    private WorkflowTemplateService workflowTemplateService;
    @Autowired
    private WorkflowProcessingService workflowProcessingService;
    @Autowired
    private UserService userService;
    @Autowired
    private ModelService modelService;

    private ProductDAO productDAO;

    public void startChangeWorkflow(final ProductModel product) {

        final WorkflowTemplateModel workflowTemplate = this.workflowTemplateService.getWorkflowTemplateForCode("ChangeProductData");
        final WorkflowModel workflow = this.workflowService.createWorkflow(workflowTemplate, product, userService.getAdminUser());

        modelService.save(workflow);

        for (final WorkflowActionModel action : workflow.getActions()) {
            modelService.save(action);
        }

        this.workflowProcessingService.startWorkflow(workflow);
    }

    public List<ProductModel> getAllProducts() {
        final List<ProductModel> result = productDAO.getAllProducts();

        if (result.isEmpty()) {
            throw new UnknownIdentifierException("Products not found!");
        }

        return result;
    }

    @Required
    public void setProductDAO(final ProductDAO productDAO) {
        this.productDAO = productDAO;
    }
}
