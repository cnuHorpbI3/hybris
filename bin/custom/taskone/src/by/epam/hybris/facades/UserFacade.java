package by.epam.hybris.facades;

import by.epam.hybris.data.UserData;

import java.util.List;

public interface UserFacade {

    List<UserData> getUsersForName(String name);

    List<UserData> getAllUsers();
}
