package by.epam.hybris.facades.impl;

import by.epam.hybris.services.UserService;
import by.epam.hybris.data.UserData;
import by.epam.hybris.facades.UserFacade;
import de.hybris.platform.core.model.user.UserModel;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.List;

public class DefaultUserFacade implements UserFacade {

    private UserService userService;

    @Override
    public List<UserData> getAllUsers() {
        final List<UserModel> userModels = userService.getUsers();
        final List<UserData> userFacadeData = new ArrayList<>();

        for (UserModel userModel : userModels) {
            final UserData userData = new UserData();
            userData.setName(userModel.getName());
            userData.setUid(userModel.getUid());
            userFacadeData.add(userData);
        }

        return userFacadeData;
    }

    @Override
    public List<UserData> getUsersForName(String name) {
        List<UserData> userDatas = null;

        if (name != null && !StringUtils.EMPTY.equals(name)) {
            List<UserModel> users = userService.getUsersForName(name);

            if (!users.isEmpty()) {
                userDatas = new ArrayList<>();
                for (UserModel userModel : users) {
                    UserData userData = new UserData();
                    userData.setName(userModel.getName());
                    userData.setUid(userModel.getUid());
                    userDatas.add(userData);
                }
            }
        } else {
            throw new IllegalArgumentException("User with name " + name + " not found.");
        }

        return userDatas;
    }

    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }
}
