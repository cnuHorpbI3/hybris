package by.epam.hybris;/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;


public class DynamicCustomDescriptionHandler implements DynamicAttributeHandler<String, CustomerModel>
{
    public static final String DELIMITER = ";";


    @Override
    public String get(final CustomerModel item)
    {
        if (item == null)
        {
            throw new IllegalArgumentException("Item model is required");
        }
        return item.getName() + DELIMITER + item.getEmail();
    }

    @Override
    public void set(final CustomerModel item, final String value)
    {
        if (item != null && value != null)
        {
            final String[] split = value.split(DELIMITER);
            item.setName(split[0]);
            item.setEmail(split[1]);
        }
    }

}