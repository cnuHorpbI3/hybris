package by.epam.hybris.jobs;

import by.epam.hybris.services.MyProductService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.log4j.Logger;

import java.util.List;

public class SendProductAmountJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(SendProductAmountJob.class.getName());

    private MyProductService productService;

    @Override
    public PerformResult perform(final CronJobModel cronJob)
    {
        LOG.info("Sending product amount");
        final List<ProductModel> products = productService.getAllProducts();

        LOG.info(products.size());

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    public void setProductService(final MyProductService productService) {
        this.productService = productService;
    }
}
