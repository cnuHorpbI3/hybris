package by.epam.hybris.interceptor.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.apache.log4j.Logger;


public class DefaultProductLoadInterceptor implements PrepareInterceptor<ProductModel> {

    private static final Logger LOGGER = Logger.getLogger(DefaultProductLoadInterceptor.class.getName());

    @Override
    public void onPrepare(final ProductModel model, final InterceptorContext ctx) throws InterceptorException {

        if (model.getDescription().contains("_")) {
            LOGGER.info(model.getDescription());
        }

    }
}
