/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Feb 9, 2017 5:06:25 PM                      ---
 * ----------------------------------------------------------------
 */
package by.epam.hybris.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedTaskoneConstants
{
	public static final String EXTENSIONNAME = "taskone";
	public static class TC
	{
		public static final String IMMUTABLEDESCRIPTIONNOTIFICATIONTYPE = "ImmutableDescriptionNotificationType".intern();
		public static final String NOTIFICATIONTYPE = "NotificationType".intern();
	}
	public static class Attributes
	{
		public static class Customer
		{
			public static final String CUSTOMERDESCRIPTION = "customerDescription".intern();
			public static final String EMAIL = "email".intern();
		}
		public static class Product
		{
			public static final String ADDITIONALDESCRIPTION = "additionalDescription".intern();
		}
	}
	
	protected GeneratedTaskoneConstants()
	{
		// private constructor
	}
	
	
}
