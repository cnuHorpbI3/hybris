/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Feb 9, 2017 5:06:25 PM                      ---
 * ----------------------------------------------------------------
 */
package by.epam.hybris.jalo;

import by.epam.hybris.constants.TaskoneConstants;
import by.epam.hybris.jalo.ImmutableDescriptionNotificationType;
import by.epam.hybris.jalo.NotificationType;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.extension.Extension;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.jalo.user.Customer;
import de.hybris.platform.jalo.user.User;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type <code>TaskoneManager</code>.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedTaskoneManager extends Extension
{
	protected static final Map<String, Map<String, AttributeMode>> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, Map<String, AttributeMode>> ttmp = new HashMap();
		Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put("additionalDescription", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.product.Product", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("email", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.user.Customer", Collections.unmodifiableMap(tmp));
		DEFAULT_INITIAL_ATTRIBUTES = ttmp;
	}
	@Override
	public Map<String, AttributeMode> getDefaultAttributeModes(final Class<? extends Item> itemClass)
	{
		Map<String, AttributeMode> ret = new HashMap<>();
		final Map<String, AttributeMode> attr = DEFAULT_INITIAL_ATTRIBUTES.get(itemClass.getName());
		if (attr != null)
		{
			ret.putAll(attr);
		}
		return ret;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.additionalDescription</code> attribute.
	 * @return the additionalDescription
	 */
	public String getAdditionalDescription(final SessionContext ctx, final Product item)
	{
		return (String)item.getProperty( ctx, TaskoneConstants.Attributes.Product.ADDITIONALDESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.additionalDescription</code> attribute.
	 * @return the additionalDescription
	 */
	public String getAdditionalDescription(final Product item)
	{
		return getAdditionalDescription( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.additionalDescription</code> attribute. 
	 * @param value the additionalDescription
	 */
	public void setAdditionalDescription(final SessionContext ctx, final Product item, final String value)
	{
		item.setProperty(ctx, TaskoneConstants.Attributes.Product.ADDITIONALDESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.additionalDescription</code> attribute. 
	 * @param value the additionalDescription
	 */
	public void setAdditionalDescription(final Product item, final String value)
	{
		setAdditionalDescription( getSession().getSessionContext(), item, value );
	}
	
	public ImmutableDescriptionNotificationType createImmutableDescriptionNotificationType(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TaskoneConstants.TC.IMMUTABLEDESCRIPTIONNOTIFICATIONTYPE );
			return (ImmutableDescriptionNotificationType)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ImmutableDescriptionNotificationType : "+e.getMessage(), 0 );
		}
	}
	
	public ImmutableDescriptionNotificationType createImmutableDescriptionNotificationType(final Map attributeValues)
	{
		return createImmutableDescriptionNotificationType( getSession().getSessionContext(), attributeValues );
	}
	
	public NotificationType createNotificationType(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TaskoneConstants.TC.NOTIFICATIONTYPE );
			return (NotificationType)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating NotificationType : "+e.getMessage(), 0 );
		}
	}
	
	public NotificationType createNotificationType(final Map attributeValues)
	{
		return createNotificationType( getSession().getSessionContext(), attributeValues );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.email</code> attribute.
	 * @return the email
	 */
	public String getEmail(final SessionContext ctx, final Customer item)
	{
		return (String)item.getProperty( ctx, TaskoneConstants.Attributes.Customer.EMAIL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.email</code> attribute.
	 * @return the email
	 */
	public String getEmail(final Customer item)
	{
		return getEmail( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.email</code> attribute. 
	 * @param value the email
	 */
	public void setEmail(final SessionContext ctx, final Customer item, final String value)
	{
		item.setProperty(ctx, TaskoneConstants.Attributes.Customer.EMAIL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.email</code> attribute. 
	 * @param value the email
	 */
	public void setEmail(final Customer item, final String value)
	{
		setEmail( getSession().getSessionContext(), item, value );
	}
	
	@Override
	public String getName()
	{
		return TaskoneConstants.EXTENSIONNAME;
	}
	
}
