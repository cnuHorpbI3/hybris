/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Feb 9, 2017 5:06:25 PM                      ---
 * ----------------------------------------------------------------
 */
package by.epam.hybris.jalo;

import by.epam.hybris.constants.TaskoneConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link by.epam.hybris.jalo.NotificationType NotificationType}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedNotificationType extends GenericItem
{
	/** Qualifier of the <code>NotificationType.name</code> attribute **/
	public static final String NAME = "name";
	/** Qualifier of the <code>NotificationType.frequency</code> attribute **/
	public static final String FREQUENCY = "frequency";
	/** Qualifier of the <code>NotificationType.shortDescription</code> attribute **/
	public static final String SHORTDESCRIPTION = "shortDescription";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(NAME, AttributeMode.INITIAL);
		tmp.put(FREQUENCY, AttributeMode.INITIAL);
		tmp.put(SHORTDESCRIPTION, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>NotificationType.frequency</code> attribute.
	 * @return the frequency
	 */
	public String getFrequency(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FREQUENCY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>NotificationType.frequency</code> attribute.
	 * @return the frequency
	 */
	public String getFrequency()
	{
		return getFrequency( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>NotificationType.frequency</code> attribute. 
	 * @param value the frequency
	 */
	public void setFrequency(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FREQUENCY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>NotificationType.frequency</code> attribute. 
	 * @param value the frequency
	 */
	public void setFrequency(final String value)
	{
		setFrequency( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>NotificationType.name</code> attribute.
	 * @return the name
	 */
	public String getName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, NAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>NotificationType.name</code> attribute.
	 * @return the name
	 */
	public String getName()
	{
		return getName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>NotificationType.name</code> attribute. 
	 * @param value the name
	 */
	public void setName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, NAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>NotificationType.name</code> attribute. 
	 * @param value the name
	 */
	public void setName(final String value)
	{
		setName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>NotificationType.shortDescription</code> attribute.
	 * @return the shortDescription
	 */
	public String getShortDescription(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHORTDESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>NotificationType.shortDescription</code> attribute.
	 * @return the shortDescription
	 */
	public String getShortDescription()
	{
		return getShortDescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>NotificationType.shortDescription</code> attribute. 
	 * @param value the shortDescription
	 */
	public void setShortDescription(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHORTDESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>NotificationType.shortDescription</code> attribute. 
	 * @param value the shortDescription
	 */
	public void setShortDescription(final String value)
	{
		setShortDescription( getSession().getSessionContext(), value );
	}
	
}
