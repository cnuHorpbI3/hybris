package by.epam.hybris.daos.impl;


import by.epam.hybris.daos.UserDAO;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DefaultUserDAOIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private UserDAO userDAO;

    @Resource
    private ModelService modelService;

    private static final String USER_NAME = "Jack";
    private static final String USER_UID = "29";

    @Test
    public void userDAOTest() {
        List<UserModel> usersByName = userDAO.findUsersByName(USER_NAME);
        assertTrue("No User should be returned", usersByName.isEmpty());

        List<UserModel> allUsers = userDAO.findUsers();
        final int size = allUsers.size();

        final UserModel userModel = new UserModel();
        userModel.setName(USER_NAME);
        userModel.setUid(USER_UID);
        modelService.save(userModel);

        allUsers = userDAO.findUsers();
        assertEquals(size + 1, allUsers.size());
        assertEquals("Unexpected user found", userModel, allUsers.get(allUsers.size() - 1));

        usersByName = userDAO.findUsersByName(USER_NAME);
        assertEquals("Did not find the User we just saved", 1, usersByName.size());
        assertEquals("Retrieved User's name attribute incorrect", USER_NAME, usersByName.get(0).getName());
    }

    @Test
    public void testFindUsersEmptyStringParam() {
        final List<UserModel> users = userDAO.findUsersByName("");
        assertTrue("No User should be returned", users.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindUsersNullParam() {
        userDAO.findUsersByName(null);
    }
}
