package by.epam.hybris.impl;

import by.epam.hybris.daos.UserDAO;
import by.epam.hybris.services.impl.DefaultUserService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.UserModel;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
public class DefaultUserServiceUnitTest {

    private DefaultUserService defaultUserService;
    private UserDAO userDAO;

    private UserModel userModel;

    private static final String USER_NAME = "Jack";
    private static final String USER_UID = "29";

    @Before
    public void setUp() {
        defaultUserService = new DefaultUserService();

        userDAO = mock(UserDAO.class);

        defaultUserService.setUserDAO(userDAO);


        userModel = new UserModel();
        userModel.setName(USER_NAME);
        userModel.setUid(USER_UID);
    }


    @Test
    public void testGetAllUsers() {
        final List<UserModel> userModels = Arrays.asList(userModel);

        when(userDAO.findUsers()).thenReturn(userModels);

        final List<UserModel> result = defaultUserService.getUsers();

        assertEquals("We should find one", 1, result.size());
        assertEquals("And should equals what the mock returned", userModel, result.get(0));
    }

    @Test
    public void testGetUser() {
        when(userDAO.findUsersByName(USER_NAME)).thenReturn(Collections.singletonList(userModel));
        final List<UserModel> result = defaultUserService.getUsersForName(USER_NAME);
        assertEquals("User should equals() what the mock returned", userModel, result.get(0));
    }
}
