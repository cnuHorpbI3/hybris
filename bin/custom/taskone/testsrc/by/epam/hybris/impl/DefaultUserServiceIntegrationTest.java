package by.epam.hybris.impl;

import by.epam.hybris.services.UserService;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@IntegrationTest
public class DefaultUserServiceIntegrationTest extends ServicelayerTransactionalTest {

    @Resource(name = "customUserService")
    private UserService customUserService;

    @Resource
    private ModelService modelService;

    private UserModel userModel;
    private final static String USER_NAME = "Jack";
    private final static String USER_UID = "29";

    @Before
    public void setUp() {
        userModel = new UserModel();
        userModel.setName(USER_NAME);
        userModel.setUid(USER_UID);
    }

    @Test(expected = UnknownIdentifierException.class)
    public void testFailBehavior() {
        customUserService.getUsersForName(USER_NAME);
    }

    @Test
    public void testUserService() {
        List<UserModel> userModels = customUserService.getUsers();
        final int size = userModels.size();

        modelService.save(userModel);

        userModels = customUserService.getUsers();
        assertEquals(size + 1, userModels.size());
        assertEquals("Unexpected user found", userModel, userModels.get(userModels.size() - 1));

        final List<UserModel> persistedUserModel = customUserService.getUsersForName(USER_NAME);
        assertNotNull("No user found", persistedUserModel);
        assertEquals("Different user found", userModel, persistedUserModel.get(0));
    }
}
