package by.epam.hybris.facades.impl;

import by.epam.hybris.data.UserData;
import by.epam.hybris.facades.UserFacade;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DefaultUserFacadeIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private UserFacade userFacade;

    @Resource
    private ModelService modelService;

    private UserModel userModel;
    private static final String USER_NAME = "Jack";
    private static final String USER_UID = "29";

    @Before
    public void setUp() {
        userModel = new UserModel();
        userModel.setName(USER_NAME);
        userModel.setUid(USER_UID);
    }

    @Test(expected = UnknownIdentifierException.class)
    public void testInvalidParameter() {
        userFacade.getUsersForName(USER_NAME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullParameter() {
        userFacade.getUsersForName(null);
    }

    @Test
    public void testUserFacade() {
        List<UserData> userListData = userFacade.getAllUsers();
        assertNotNull(userListData);
        final int size = userListData.size();
        modelService.save(userModel);

        userListData = userFacade.getAllUsers();
        assertNotNull(userListData);
        assertEquals(size + 1, userListData.size());
        assertEquals(USER_NAME, userListData.get(size).getName());
        assertEquals(USER_UID, userListData.get(size).getUid());

        final List<UserData> persistedUserData = userFacade.getUsersForName(USER_NAME);
        assertNotNull(persistedUserData);
        assertEquals(USER_NAME, persistedUserData.get(0).getName());
        assertEquals(USER_UID, persistedUserData.get(0).getUid());
    }


}
