package by.epam.hybris.facades.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import by.epam.hybris.data.UserData;
import by.epam.hybris.services.UserService;
import de.hybris.platform.core.model.user.UserModel;
import org.junit.Before;
import org.junit.Test;

public class DefaultUserFacadeUnitTest {

    private DefaultUserFacade userFacade;

    private UserService userService;

    private final static String USER_NAME = "Jack";
    private final static String USER_UID = "29";

    private List<UserModel> dummyDataUserList() {
        final UserModel jack = new UserModel();
        jack.setName(USER_NAME);
        jack.setUid(USER_UID);
        final List<UserModel> users = new ArrayList<>();
        users.add(jack);
        return users;
    }

    private UserModel dummyDataUser() {
        final UserModel jack = new UserModel();
        jack.setName(USER_NAME);
        jack.setUid(USER_UID);
        return jack;
    }

    @Before
    public void setUp() {
        userFacade = new DefaultUserFacade();
        userService = mock(UserService.class);

        userFacade.setUserService(userService);
    }
    @Test
    public void testGetAllUsers() {
        final List<UserModel> users = dummyDataUserList();
        final UserModel jack = dummyDataUser();

        when(userService.getUsers()).thenReturn(users);

        final List<UserData> dto = userFacade.getAllUsers();

        assertNotNull(dto);
        assertEquals(users.size(), dto.size());
        assertEquals(jack.getName(), dto.get(0).getName());
        assertEquals(jack.getUid(), dto.get(0).getUid());
    }

    @Test
    public void testGetUsersForName() {
        final List<UserModel> users = dummyDataUserList();

        when(userService.getUsersForName(USER_NAME)).thenReturn(users);

        final List<UserData> dto = userFacade.getUsersForName(USER_NAME);

        assertNotNull(dto);
        assertEquals(users.size(), dto.size());
    }
}
